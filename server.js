const express = require('express');
const { resolve } = require('path');
const app = express();
const { port } = require('./config');

app.use(express.static(resolve('public')));

app.get('*', (req, res) => {
    res.sendFile('/index.html');
});

app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});